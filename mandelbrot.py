import numpy as np
import matplotlib.animation as animation 
import matplotlib.pyplot as plt
from matplotlib import colors
from PIL import Image

def mandelbrot(n_rows: int, n_columns: int, iterations: int):
    y_len = int(n_columns/2)
    x_cor = np.linspace(-2, 0.48, n_rows)
    y_cor = np.linspace(0, 1.24, y_len)

    output = np.zeros((n_rows, y_len), dtype=np.uint8)
    elements_todo = np.ones((n_rows, y_len), dtype=bool) # a bool-mask 
    z = np.zeros((n_rows, y_len), dtype = complex)
    c = np.zeros((n_rows, y_len), dtype = complex) # cor is the coordinate array containing each pixel
    c.imag, c.real = np.meshgrid(y_cor, x_cor)

    for iteration in range(1, iterations + 1):
        z[elements_todo] = (z[elements_todo]**2 + c[elements_todo])
        mask = np.logical_and((z.real**2 + z.imag**2) > 4, elements_todo)
        elements_todo = np.logical_and(elements_todo, np.logical_not(mask))
        output[mask] = iteration

    final_out = np.hstack((np.fliplr(output[:,1:]), output)).T
    return final_out


plt.gray()

fig = plt.figure(figsize=(10, 10))  # instantiate a figure to draw
ax = plt.axes()  # create an axes object

m_b =  mandelbrot(1500,1500,1000)
light = colors.LightSource(azdeg=315, altdeg=10)
mandel = light.shade(m_b, cmap=plt.cm.gist_earth, vert_exag=1.0, norm=colors.PowerNorm(0.3), blend_mode='hsv')
mandel2 = light.shade(m_b, cmap=plt.cm.gist_earth, vert_exag=0.25, norm=colors.PowerNorm(0.55), blend_mode='soft')

plt.imsave("Mandelbrot1-t.png", mandel, dpi=1200)
plt.imsave("Mandelbrot2-t.png", mandel2, dpi=1200)

print("done")

# animation currently not in use
'''def animate(i):
    ax.clear()  # clear axes object
    output = mandelbrot(i*2+1,i*2+1,25,4*i/frames_count) 
    print(100*i/frames_count, "%")
    img = ax.imshow(output.T, interpolation="none", cmap='Greys_r')
    return [img]



frames_count = 750

anim = animation.FuncAnimation(fig, animate, frames=frames_count, interval=120, blit=True)

Writer = animation.writers['ffmpeg']
writer = Writer(fps=30, metadata={'artist': 'Jan Förster'}, bitrate=1800)

anim.save('mandeltest.mp4', writer = writer)'''